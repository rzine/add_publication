---
title: "Référencer une publication sur Rzine"
subtitle: "Quels sont les fichiers à fournir pour référencer une ressource sur Rzine"
author: <img src="figure/Bandeau_rzine_small.png" width="90">
date: "`r Sys.Date()`"
css: styles.css
output:
  rmdformats::readthedown
---

```{r, setup, eval=TRUE, include=FALSE}
knitr::opts_chunk$set(message=FALSE, warning=FALSE)

```

> Rzine permet de référencer et de diffuser de la documentation de qualité sur R, quelque soit le format (article, diaporama, exercice, cours, manuel, site web, application...).     
> La plateforme vous permet d’héberger le contenu, à l'exception des vidéos et des sites web.     
> Pour référencez une publication, vous devez au minimum fournir un fichier markdown comportant les métadonnées de votre publication.

<br>


## Introduction

Pour référencer une ressource sur Rzine, il suffit de **renseigner un fichier balisé en Markdown** (<span class="meta">index.md</span>) dans lequel sont renseignées différentes **métadonnées qui décrivent et caractérisent la ressource**. Une **illustration** (<span class="meta">featured.png</span>), et un **fichier BibTex** (<span class="meta">cite.bib</span>) peuvent être associés à ce markdown.

**Ces différents fichiers doivent être nommés et structurés de manière précise**. 

<br>

<p class="center"> [<img src="figure/zip.png" width="50"/> **Télécharger le modèle**](https://gitlab.huma-num.fr/rzine/add_publication/-/raw/master/20191216_Publication_name.zip) </p>

<br>
      
Décompressez l'archive, **puis commencez par renommer le répertoire** <span class="meta">20191216_Publication_name</span> **de façon explicite**. Utilisez la **date** et des **mots clefs**. Exemple :

<br>

<p class="center">
![](figure/pub_name.png?style=centerme){width=350px}
</p>

<br>
<br>

<div class="row">
<div class="column3">

**Ce répertoire contient trois fichiers par défaut** :

- **cite.bib** : [référence bibliographique format BibTeX](https://fr.wikipedia.org/wiki/BibTeX){target="_blank"}
- **featured.png** : illustration  
- **index.md** : markdown de métadonnées
  

</div>
<div class="column2">
<p class="center">
![](figure/pub_file.png?style=centerme){width=170px}
</p>
</div>
</div>

<br>

<div class="alert alert-danger" role="alert">
**Suivez les instructions ci-dessous pour compléter et modifier les différents fichiers**.
</div>

<br>


## index.md

Editer le fichier <span class="meta">index.md</span> avec n'importe quel éditeur de texte.

<p class="center">
![](figure/fiche_publi_modele.PNG?style=centerme){width=650px} 
</p>

<br>

**MÉTADONNÉES OBLIGATOIRES** :    

- <span class="meta">authors</span> : **Username** de(s) auteur(s
- <span class="meta">title</span> :le **titre**     
- <span class="meta">summary</span> : Un **résumé court** (phrase d'accroche)  
- <span class="meta">date</span> : la **date** 
- <span class="meta">publication_types</span> : le **type** de publication
- <span class="meta">thematics</span> : Une **thématique**
- <span class="meta">languages</span> : la **langue** utilisée
- <span class="meta">update</span> : **Ressource maintenue à jour ?**
- <span class="meta">url_source</span> : **lien vers la ressource** (URL web ou chemin interne)
- <span class="meta">tags</span> : une liste de **mots clefs**


**MÉTADONNÉES OPTIONNELLES :** 

- <span class="meta">subtitle</span> : un **sous-titre** 
- <span class="meta">url_code</span>, <span class="meta">url_dataset</span>, <span class="meta">url_pdf</span>, <span class="meta">url_slide</span>... : des **liens** vers des ressources associées
- <span class="meta">doi</span> : un **DOI**, identifiant numérique de la publication
- <span class="meta">sources</span> : Un **projet source**

**Si le document a été ou sera publié dans une revue :**

-  <span class="meta">publishDate</span> : la **date de publication** dans la revue   
-  <span class="meta">publication</span> : la **référence complète de la revue**  


Il est aussi possible d'ajouter une **présentation détaillée** de la ressource. La longueur est illimitée. la mise en forme se fait en syntaxe markdown.

<br>

<div class="alert alert-danger" role="alert">
**Supprimez ou passez en commentaire** (**#**) les métadonnées que vous ne souhaitez pas utiliser.
</div>

<br>

### authors

Indiquez le ou les **username** de(s) auteur(s) de la publication. Le **username** est déclaré lors du référencement d'un auteur sur rzine.

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
authors:
- jdupont70
```


<div class="alert alert-danger" role="alert">
A. **Vous êtes déjà référencé.e.s comme auteur.e** sur Rzine. **Retrouvez votre username** déclaré dans [**cette liste**](https://gitlab.huma-num.fr/rzine/add_author/-/raw/master/List_username.txt){target="_blank"}      
B. **Vous n'êtes pas encore référencé.e.s** sur Rzine. **Créez votre fiche** auteur en suivant [**ces instructions**](https://rzine.gitpages.huma-num.fr/add_author/){target="_blank"}     
C. **Vous ne souhaitez pas être référencé.e sur Rzine**. Dans ce cas, **indiquez directement prénom et nom**. Ex : </div>   

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
authors:
- Jeanne Dupont
```


En cas de publication collective, **indiquez un.e auteur.e par ligne** :


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
authors:
- jdupont70
- Evangelina Klein
- Rbourdoin81
```


<br>



### title


Indiquez le **titre** (court) de la ressource avec <span class="meta">title</span>. 

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
title: Analyse de réseau avec R
```

<div class="alert alert-danger" role="alert">
**N'utilisez jamais les caractères ":" et  "'" dans la chaîne de caractère**. Cela empêche le parsage correct des métadonnées. **Cette consigne est à respecter pour toutes les variables !**
</div>

<br>

### summary


Écrivez un **texte court** qui **présente brièvement la ressource**. **170 caractères maximum !**


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
summary: Support de formation réalisé dans le cadre d un atelier organisé par l axe territoire et transport [UMR 4356]
```
<br>

### sub-title

**Métadonnée optionnelle**. Supprimez (ou passez en commentaire) cette variable si vous ne souhaitez pas l'utiliser.



```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
subtitle: Mesure et représentation avec le package igraph
```

<br>

### date


Indiquez la **date entre guillemets**  et correctement formatée : **"yyyy-mm-dd"**

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
date: "2019-12-02"
```

<br>

### publication_types

Cette variable permet de classifier les ressources par type de ressource. **La classification proposée par Rzine est la suivante** :

- <span class="meta">"0"</span> -  **Agrégateur et liste**
- <span class="meta">"1"</span> -  **Application**
- <span class="meta">"2"</span> -  **Article (inclut fiche Rzine)**
- <span class="meta">"3"</span> -  **Billet** 
- <span class="meta">"4"</span> -  **Communication**
- <span class="meta">"5"</span> -  **Cours et exercice**
- <span class="meta">"6"</span> -  **Livre et manuel** 
- <span class="meta">"7"</span> -  **Package et vignette**
- <span class="meta">"8"</span> -  **Sites internet**
- <span class="meta">"9"</span> -  **Vidéos (inclut les communications (diapo) associées à une vidéo)**  



**Indiquez l'identifiant correspondant entre guillemets**.


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
publication_types:
- "3"
```

Ou plusieurs entrées :

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
publication_types: ["3", "9"]
```


<br>



### thematics


Vous pouvez **consulter la classification thématique** actuelle à [**ce lien**](https://gitlab.huma-num.fr/rzine/add_publication/-/raw/master/List_thematics.txt){target="_blank"}. **Indiquez l'identifiant correspondant entre guillemets**.


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
thematics:
- "0"
```

Ou plusieurs entrées :

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
thematics: ["2", "20"]
```


<br>

### languages


Précisez la **langue utilisée** :

- <span class="meta">"0"</span> -  **Français**
- <span class="meta">"1"</span> -  **Anglais**
- <span class="meta">"2"</span> -  **Espagnol**

**Indiquez l'identifiant correspondant** entre guillemets.


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
languages:
- "0"
```

<br>


### update

**La ressource est-elle susceptible d'être mise à jour ?**

- <span class="meta">"0"</span> -  **non**
- <span class="meta">"1"</span> -  **susceptible d'être mis à jour**

**Indiquez l'identifiant correspondant** entre guillemets.

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
update:
- "0"
```

<br>

### sources


**Métadonnée optionnelle**. Indiquez la **source**. Trois possibilités :

- <span class="meta">"0"</span> -  **Divers**
- <span class="meta">"1"</span> -  **AfrimapR**
- <span class="meta">"2"</span> -  **O'Reilly**
- <span class="meta">"3"</span> -  **Parcours R**
- <span class="meta">"4"</span> -  **UtilitR**
- <span class="meta">"5"</span> -  **rOpenSci**
- <span class="meta">"6"</span> -  **R series **
- <span class="meta">"7"</span> -  **RUSS**
- <span class="meta">"8"</span> -  **Rzine**
- <span class="meta">"9"</span> -  **SIGR 2021**



```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
sources:
- "0"
```

<br>


### url_source


**Indiquez l'URL d’accès** à la ressource, **entre guillemets**.

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
url_source : 'https://gitlab.huma-num.fr/atelier/igraph/analyse_reseau_avec_r.html'
```

<div class="alert alert-danger" role="alert">
**Si vous souhaitez faire héberger votre ressource documentaire par Rzine**. Indiquez l'URL d’accès (locale) à la ressource, **en respectant la syntaxe suivante** : ***docs/nom_du_repertoire/nom_de_la_ressource.pdf***

Seules les ressources en format **pdf** et **html** sont acceptées.
</div>


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
url_source : 'docs/20191202_analyse_reseau_avec_r_dupont/analyse_reseau_avec_r.html'
```



<br>


### url_


Il est également possible d'**ajouter un certains nombre de liens vers des ressources associées** (**optionnel**) :


- <span class="meta">url_code</span> : Lien vers le **code source** de la ressource    
- <span class="meta">url_dataset</span> : Lien vers un **jeu de données** utilisés 
- <span class="meta">url_pdf</span> : Lien vers une version **pdf** 
- <span class="meta">url_project</span> : Lien vers le site du projet
- <span class="meta">url_slides</span> : Lien vers des diapositives
- <span class="meta">url_video</span> : Lien vers une vidéo
- <span class="meta">url_poster</span> : Lien vers un poster

*Exemple :* 

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}

# Code source stocké sur GIT
url_code: 'https://gitlab.huma-num.fr/atelier/igraph/'

# Jeu de données
url_dataset: 'https://gitlab.huma-num.fr/atelier/igraph/raw/master/data.zip?inline=false'

# version  PDF  
url_pdf: 'https://gitlab.huma-num.fr//atelier/igraph/analyse_reseau_avec_r.pdf'


```

<div class="alert alert-danger" role="alert">
Indiquez les URL **entre guillemets**.  


Dans le cas ou vous souhaitez faire héberger ces ressources par Rzine, **indiquez l'URL d’accès locale**. Seules les ressources en format **pdf**, **html** et **zip** sont acceptées.
</div>


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}



# Code source stocké sur GIT
url_code: 'docs/20191202_analyse_reseau_avec_r_dupont/analyse_reseau_avec_r.rmd'

# Jeu de données
url_dataset: 'docs/20191202_analyse_reseau_avec_r_dupont/data.zip'

# version  PDF  
url_pdf: 'docs/20191202_analyse_reseau_avec_r_dupont/analyse_reseau_avec_r.pdf'


```

<br>

### DOI

Indiquez si votre ressource possèdent un **identifiant numérique d'objet** (**optionnel**). 

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
doi: '10.1007/s00223-003-0070-0'
```



### tags

**Les mots clefs sont cruciaux pour l'indexation et la visibilité de votre publication** sur Rzine. Renseignez-les de la façon suivante :

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}

tags:
- igraph
- réseau
- atelier
- tranport

```


<div class="alert alert-danger" role="alert">
**Vous êtes libre de choisir les tags que vous souhaitez, le nombre n'est pas limité**. N'hésitez pas à **citer des packages**, et/ou à utiliser l'anglais.
</div>

<br>




### publication

Si votre publication a fait ou fera l'objet d'une publication dans une revue, Vous pouvez renseigner cela avec les variables suivantes (**optionnel**) :

- <span class="meta">publishDate</span> : **Date de publication** dans la revue. Ex : *publishDate: "2019-09-08"*
- <span class="meta">publication</span> : **Nom de la revue**. Ex : *publication: L'Espace géographique*


Si vous ne souhaitez pas utiliser ces métadonnées, **supprimez ces lignes ou passez-les en commentaire**.

<br>

### Texte libre

Il n'est **pas obligatoire**, **mais conseillé**, d'ajouter un résumé long qui présente et contextualise votre ressource documentaire.

Pour cela, ajoutez du texte [formaté en markdown](https://www.markdownguide.org/basic-syntax/){target="_blank"}, dans le corps du fichier. Ex : 


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
---
authors:
- jdupont70

title: Analyse de réseau avec R
summary: Support de formation réalisé dans le cadre d un atelier organisé par l axe territoire et transport [UMR 4356]

date: "2019-12-02"

publication_types:
- "3"

thematics:
- "0"

languages:
- "0"

update:
- "0"

sources:
- "0"

url_source : 'https://gitlab.huma-num.fr/atelier/igraph/analyse_reseau_avec_r.html'

tags:
- igraph
- réseau
- atelier
- tranport

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

### Title level-3

Rédigé ici votre résumé/texte libre qui présente et contextualise votre [publication](link).
Utilisez la **syntaxe markdown** pour mettre en forme votre texte.

#### Title level-4

Le longueur de ce texte n est pas limité. *Expression libre*.
  
```

<br>

## cite.bib


Si vous souhaitez mettre à disposition la [référence bibliographique au format BibTeX](https://fr.wikipedia.org/wiki/BibTeX){target="_blank"}, **modifiez le fichier** ***cite.bib***  

  
<p class="center">
![](figure/citebib.png?style=centerme){width=440px}
</p>


<div class="alert alert-danger" role="alert">
**Ne le renommer pas**. **Supprimez-le si vous ne souhaitez pas profiter de cette fonctionnalité**.
</div>



<br>

## featured.png


Votre publication sera illustrée **par défaut**. **Remplacez cette image par celle de votre choix**.

<p class="center">
![](figure/public1.png?style=centerme){width=350px}
![](figure/arrow2.png?style=centerme){width=40px} 
![](figure/public3.png?style=centerme){width=330px} 
</p>


<div class="alert alert-danger" role="alert">
**Votre illustration doit obligatoirement être nommée** <span class="meta">featured</span>. Les formats **jpg**, **jpeg** ou **png** sont acceptés.

**Veillez à ne pas dépasser la dimension** : <span class="meta">1000x1000</span> **pixels**.

</div>

<br>

Pour **ajouter un crédit sur votre illustration**. Utilisez la variable <span class="meta">caption</span> dans le fichier <span class="meta">index.md</span>


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}

# Image credit
image:
  caption: '© Jeanne Dupont'
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify
  
```


<br>



## Archive à soumettre

votre répertoire doit comporter **au moins 2 éléments** : 


- **Le fichier** <span class="meta">index.md</span> **complété**
- **Une illustration** nommée <span class="meta">featured.png</span> (ou **.jpg**, **.jpeg**) 
- **En option :** la référence de votre publication en format **BibTex**, <span class="meta">cite.bib</span>



<div class="alert alert-danger" role="alert">
**Si vous souhaitez faire héberger la publication et ses ressources associées** par Rzine, **ajoutez les différents fichiers à la racine du répertoire**. **Tous les fichiers supplémentaires se doivent d'être appelés dans le fichier <span class="meta">index.md</span>** ->  **cf** [***url_***](#url_)
</div>


<p class="center">
![](figure/full_archive.PNG?style=centerme){width=550px}
</p>    


<br>

**Compressez votre répertoire**. **Votre archive est prête !**

<br>


<p class="center">
![](figure/archive.PNG?style=centerme){width=210px}
</p>

<br>

<div class="alert alert-info" role="alert">
**Envoyez votre archive à l'adresse suivante** <span class="meta">**contact@rzine.fr**</span>
</div>
