0 - Premiers pas avec R
1 - Markdown (blogdown, distill, rmd...)
10 - Manipulation de données
11 - Représentation des données
12 - Stat. uni, bi et multivariées
13 - Modélisation statistique
14 - Statistique bayésienne
15 - Séries temporelles
16 - Séquences et trajectoires (analyse de séquence, longitudinale)
17 - Analyse textuelle
18 - Analyse de réseau (inclut sfnetwork)
19 - Analyse et stat. spatiales
2 - Géomatique, cartographie (géo-traitement)
20 - Analyse d'images (télédétection, raster, détection pattern...)
3 - Machine et deep learning
4 - Simulation
5 - Développement (package, shiny...)
6 - API et scraping
7 - Workflow (Git, repro, bonnes pratiques, docker, renv, packrat, connexion BD...)
8 - Communauté R (ressource collaborative, liste, groupe...)
9 - Autres




